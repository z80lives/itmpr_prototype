#include "DisplayManager.h"
#include <iostream>

DisplayManager::DisplayManager(int w, int h, int bpp)
{
    SDL_Init(SDL_INIT_VIDEO );
    window = SDL_CreateWindow("Game prototype",
                     SDL_WINDOWPOS_UNDEFINED,
                     SDL_WINDOWPOS_UNDEFINED,
                      w, h,
                      NULL);
    width = w;
    height = h;
    renderer = SDL_CreateRenderer(window, -1,
				  SDL_RENDERER_ACCELERATED
				  |
				  SDL_RENDERER_TARGETTEXTURE
				  );
    //SDL_Texture* tex = SDL_Load
    /*while(1){
        SDL_Event e;
        if(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT)
                break;
        }
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
    }*/

    //SDL_Delay(3000);
}
void DisplayManager::draw()
{
    //SDL_Rect r = {100, 100, 30 ,30};
    //SDL_RenderCopy(renderer, t, NULL, &r);
    for(Sprite* s : sprite_list){
      SDL_RenderCopy(renderer, s->getTexture(), NULL, s->getDrawRectangle());
    }


}



DisplayManager::~DisplayManager(){
    sprite_list.clear();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
