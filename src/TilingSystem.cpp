#include "TilingSystem.h"
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_image.h>

#include "Sprite.h"
#include "Tile.h"

using namespace std;

TilingSystem::TilingSystem(DisplayManager *dm) : Sprite(dm->getRenderer())
{
  SDL_Texture *t = SDL_CreateTexture(dm->getRenderer(),
				     SDL_PIXELFORMAT_RGBA8888,
				     SDL_TEXTUREACCESS_TARGET,
				     dm->getWidth(),
				     dm->getHeight());

  setTexture(t);
  display = dm;
  rect.w = dm->getWidth();
  rect.h = dm->getHeight();
  rect.x = 0;
  rect.y = 0;

  tile_w = 0;
  tile_h = 0;
  tile_count = 0;
  columns = 1;

  /* SDL_Renderer *renderer = dm->getRenderer();

  rect.w = dm->getWidth();
  rect.h = dm->getHeight();
  rect.x = 0;
  rect.y = 0;


    static SDL_Point points[4] = {
      {320, 200},
      {300, 240},
      {340, 240},
      {320, 200}
    };


    SDL_SetRenderTarget(renderer, getTexture());

    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLines(renderer, points, 4);
    SDL_RenderPresent(renderer);

    SDL_SetRenderTarget(renderer, NULL);


    dm->addSprite(this);*/
}

void TilingSystem::loadTileset(string filename){
    SDL_Renderer *renderer = display->getRenderer();

    SDL_SetRenderTarget(renderer, getTexture());

    SDL_SetRenderDrawColor(renderer, 255, 0, 255, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    Sprite spr(renderer, filename);

    for(int i=0; i<2; i++){
        Sprite *newSprite = new Tile(renderer, 128, 64,
                                      spr,
                                      0, i*128,
                                      128, 128);
        tileset.push_back(*newSprite);
    }

    SDL_Rect tile_rect = {0,0, 128, 128};
    SDL_Rect dest_rect = {300,200, 128, 128};
    //draw sprite

    Sprite::copySprite(renderer, tileset[0], *((Sprite*)this),
                       tile_rect,
                       dest_rect )  ;

    dest_rect.y += 100;

    Sprite::copySprite(renderer, tileset[1], *((Sprite*)this),
                       tile_rect,
                       dest_rect )  ;
    //display->addSprite(this);
}

TilingSystem::~TilingSystem()
{
    //dtor

  tileset.clear();
  layer.clear();
}

//void TilingSystem::onDraw(SDL_Renderer* renderer){

//}
