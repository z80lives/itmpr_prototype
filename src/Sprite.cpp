
#include <stdexcept>

#ifdef __linux__
 #include <SDL_image.h>
#else
 #include <SDL2/SDL_Image.h>
#endif

#include "Sprite.h"

Sprite* Sprite::createSpriteWithTextureAccess(SDL_Renderer* r, int w, int h){
  Sprite* ret = new Sprite(r);
  SDL_Texture *t = SDL_CreateTexture(r,
				     SDL_PIXELFORMAT_RGBA8888,
				     SDL_TEXTUREACCESS_TARGET,
				     w,
				     h);

  ret->setTexture(t);
}


void Sprite::copySprite(SDL_Renderer *renderer, Sprite& src, Sprite& dest,
			const SDL_Rect& src_rect, const SDL_Rect& dest_rect){

  SDL_Texture *t = dest.getTexture();

  //Render on texture and copy
  SDL_SetRenderTarget(renderer, t);
  SDL_SetTextureBlendMode(t, SDL_BLENDMODE_BLEND);
  SDL_RenderCopy(renderer, src.getTexture(),
		 &src_rect, &dest_rect);

  //Switch back render target
  SDL_RenderPresent(renderer);
  SDL_SetRenderTarget(renderer, NULL);
}

/**
 * Load image
 **/
void Sprite::loadImage(SDL_Renderer* renderer, std::string filename){
        SDL_Surface *img = IMG_Load(filename.c_str());
	if(img == NULL)
	  throw std::runtime_error("Cannot load image " + filename);
	rect.w = img->w;
	rect.h = img->h;

	texture = SDL_CreateTexture(renderer,
				     SDL_PIXELFORMAT_RGBA8888,
				     SDL_TEXTUREACCESS_TARGET,
				     img->w,
				     img->h);

        texture = SDL_CreateTextureFromSurface(renderer, img);

        SDL_FreeSurface(img);
}

void Sprite::setTexture(SDL_Texture* texture){
  this->texture =texture;
}


/**
 * Constructor
 **/
Sprite::Sprite(SDL_Renderer* renderer, std::string filename)
{
    rect.x =rect.w= 0;
    rect.y =rect.h= 0;
    texture = NULL;

    if(filename != ""){
        loadImage(renderer, filename);
    }

}

Sprite::~Sprite()
{
    if(texture != NULL)
        SDL_DestroyTexture(texture);
}
