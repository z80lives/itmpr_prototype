#include "GameManager.h"
#include <iostream>
#include <map>

#include "SDL2/SDL.h"
using namespace std;

static void printMe(){
    cout<<"Hei";
}

GameManager::GameManager(DisplayManager *d)
{
    display = d;
}

GameManager::~GameManager()
{
    //dtor
}


void GameManager::registerEvent(Uint32 ev, Callable& method)
{
    callback_map[ev] = method;
}



void GameManager::run()
{
    SDL_Renderer *renderer = display->getRenderer();
    SDL_bool done = SDL_FALSE;

    while(!done){
        SDL_Event ev;

        while(SDL_PollEvent(&ev)){
            if(ev.type == SDL_QUIT)
                done = SDL_TRUE;

            std::map<Uint32, Callable>::iterator entry =  callback_map.find(ev.type);
            if( entry != callback_map.end()){
                entry->second();        //Call the registered method
            }
        }
        //callback_map[ev];
        //if(a[ev])
        //    cout<<"SA";
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
	display->draw();
        SDL_RenderPresent(renderer);
    }
}
