#include "Tile.h"
#include <SDL2/SDL.h>

Tile::Tile(SDL_Renderer* r, int w, int h) : Sprite(r){
  rect.w = w;
  rect.h = h;

  SDL_Texture *t = SDL_CreateTexture(r,
				     SDL_PIXELFORMAT_RGBA8888,
				     SDL_TEXTUREACCESS_TARGET,
				     w,
				     h);
  setTexture(t);
}


Tile::Tile(SDL_Renderer* renderer, int w, int h, Sprite &src,
	   int xoff, int yoff, int clip_w, int clip_h) : Sprite(renderer){
  rect.w = w;
  rect.h = h;

  const SDL_Rect src_rect = {xoff, yoff, clip_w, clip_h};
  const SDL_Rect dest_rect = {0,0, w, h};
  rect = dest_rect;

  SDL_Texture *t = SDL_CreateTexture(renderer,
				     SDL_PIXELFORMAT_RGBA8888,
				     SDL_TEXTUREACCESS_TARGET,
				     w,
				     h);


  //Render on texture and copy
  SDL_SetRenderTarget(renderer, t);
  SDL_SetTextureBlendMode(t, SDL_BLENDMODE_BLEND);
  SDL_RenderCopy(renderer, src.getTexture(),
		 &src_rect, &dest_rect);

  //Switch back render target
  SDL_RenderPresent(renderer);
  SDL_SetRenderTarget(renderer, NULL);


  setTexture(t);

}
