#include <iostream>
//#include "LevelReader.h"

#include "GameManager.h"
//#include "Sprite.h"
#include "TilingSystem.h"

#include "DisplayManager.h"

using namespace std;

void printMe(){
    cout << "mouse clicked!"<<endl;
}

int main(int argv, char** args)
{
    DisplayManager displayManager(800, 600, 32);
    GameManager gm(&displayManager);

    Callable c = printMe;

    //Sprite* s = new Sprite(displayManager.getRenderer(), "../../data/tile-grass.png");
    //displayManager.addSprite(s);

    TilingSystem ts(&displayManager);
    ts.loadTileset("../../data/tiles.png");
    displayManager.addSprite(&ts);

    gm.registerEvent(SDL_KEYDOWN, c);
    gm.run();

    //LevelReader reader("data/tes-game.json");

    return 0;
}
