#ifndef TILE_H
#define TILE_H

#include "Sprite.h"

class Tile : public Sprite{
 public:
  Tile(SDL_Renderer* r, int w, int h);
  Tile(SDL_Renderer* r, int w, int h, Sprite &src,
       int xoff, int yoff, int clip_w, int clip_h);
  ~Tile();
};

#endif
