#ifndef DISPLAYMANAGER_H
#define DISPLAYMANAGER_H

#include <SDL2/SDL.h>

#include <vector>

#include "Sprite.h"
class DisplayManager
{
    public:
        DisplayManager(int width, int height, int bpp);
        ~DisplayManager();
        SDL_Renderer* getRenderer() const{return renderer;}
        void draw();
	int getWidth(){return width; }
	int getHeight(){return height; }
        void addSprite(Sprite* spr){sprite_list.push_back(spr);}
    protected:
        std::vector<Sprite*> sprite_list;
    private:
        SDL_Renderer* renderer;
        SDL_Window* window;
        SDL_Texture *t;
	int width, height;
};

#endif // DISPLAYMANAGER_H
