#ifndef TILINGSYSTEM_H
#define TILINGSYSTEM_H
#include "DisplayManager.h"

#include <Sprite.h>

#include <vector>

#include <string>

class TilingSystem : public Sprite
{
    public:
  /*struct Tile_Type{
    int uid;
    Uint32 w;
    Uint32 h;
    };*/
        TilingSystem(DisplayManager *dm);
        ~TilingSystem();
	void loadTileset(std::string filename);
    protected:

    private:
	std::vector<Sprite> tileset;
        std::vector<int> layer;
	DisplayManager* display;
	Uint32 tile_w, tile_h, tile_count;
	Uint32 margin, spacing, columns;
	Uint32 map_width, map_height;
};

#endif // TILINGSYSTEM_H
