#ifndef SPRITE_H
#define SPRITE_H

#include <SDL2/SDL.h>
#include <string>
//#include "DisplayManager.h"

//class DisplayManager;
class Sprite{
    public:
        Sprite(SDL_Renderer* renderer){Sprite(renderer, "");}
        Sprite(SDL_Renderer* renderer, std::string filename);
        ~Sprite();
        SDL_Texture* getTexture() const{return texture;};
        SDL_Rect* getDrawRectangle() {return &rect;}

	static Sprite* createSpriteWithTextureAccess(SDL_Renderer* r,
						     int w,
						     int h);
	static void copySprite(SDL_Renderer* r, Sprite& src, Sprite& dest,
			       const SDL_Rect& src_rect, const SDL_Rect& dest_rect);
    protected:
	void setTexture(SDL_Texture* texture);
	SDL_Rect rect;
	void loadImage(SDL_Renderer* r, std::string filename);
    private:
	SDL_Texture* texture;
        //DisplayManager* display;
};

#endif // SPRITE_H
