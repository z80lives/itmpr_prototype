#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "DisplayManager.h"
#include <map>
#include <functional>

#define fnPtr void(*callback)()

typedef std::function<void()> Callable;
class GameManager
{
    public:
        GameManager(DisplayManager* display);
        ~GameManager();
        void registerEvent(Uint32 ev, Callable& method);
        void registerEvent(Uint32 ev, fnPtr );
        void run();
    protected:

    private:
        //std::map<SDL_Event, Callable> callback_map;
        std::map<Uint32, Callable> callback_map;
        DisplayManager *display;
};

#endif // GAMEMANAGER_H
