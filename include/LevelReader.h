#ifndef LEVELREADER_H
#define LEVELREADER_H
#include <string>



#include <../json.hpp>
using json = nlohmann::json;

class LevelReader
{
    private:
        json jsonData;
    public:
        LevelReader(std::string filename);

    protected:

};

#endif // LEVELREADER_H
