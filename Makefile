MAIN_FILE = main.cpp
MODE = debug

SOURCES = $(filter-out src/$(MAIN_FILE), $(wildcard src/*.cpp))
INCLUDE_DIR = include
OBJ_DIR = obj
OBJS := $(SOURCES:src/%.cpp=$(OBJ_DIR)/%.o)
CC = g++

LIBS = sdl2 SDL2_image

DEBUG_FLAGS = -g -Wall
CFLAGS = -std=c++0x -w -I$(INCLUDE_DIR) `pkg-config --cflags $(LIBS)` $(DEBUG_FLAGS)

OUTPUT_DIR = bin/linux


LFLAGS = `pkg-config --libs $(LIBS)` 
EXEC_NAME = main

#This is the target that compiles our executable

$(OBJ_DIR)/%.o: src/%.cpp
	@echo Compiling $@
	@$(CC) -c $(CFLAGS) $< -o $@

all : $(OBJS)
	@echo Compiling and linking main file
	@$(CC) $(MAIN_FILE) $(OBJS) $(CFLAGS) $(LFLAGS) -o $(OUTPUT_DIR)/$(EXEC_NAME)
	@echo Done


clean:
	@echo Deleting object files
	@rm obj/*.o
